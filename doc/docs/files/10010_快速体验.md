# 快速体验

> 运行环境：JDK8，Maven3，Zookeeper

- 安装并启动zookeeper，[安装教程](http://zookeeper.apache.org/doc/r3.4.13/zookeeperStarted.html)
- IDE打开项目(IDEA下可以打开根pom.xml，然后open as project)
- 启动注册中心，sop-registry（运行SopRegistryApplication.java）
- 启动微服务：sop-story-web(运行SopStoryApplication.java)
- 启动网关：sop-gateway（运行SopGatewayApplication.java）
- 找到sop-test，打开测试用例，进行接口调用测试，运行com.gitee.sop.AlipayClientPostTest.testPost()

确保注册中心先启动

## 使用admin

- 找到`sop-admin/sop-admin-server`工程，运行`com.gitee.sop.adminserver.SopAdminServerApplication.java`
- 找到`sop-admin/sop-admin-front/index.html`文件，在IDEA下直接右键--Run'index.html'
- 如果没有用到IDEA，则需要把sop-admin-front放到静态服务器中然后访问index.html


